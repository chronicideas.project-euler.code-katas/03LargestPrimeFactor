import org.junit.Assert;

public class Test {

    public static void main(String[] args) {
        tests(13195, 29);
        tests(600851475143L, 6857);
    }

    public static long getPrimeFactors(long number) {
        System.out.println("\nThe given number is: " + number);

        for(int i = 2; i<=(number/i); i++) {
            while (number % i == 0) {
                number /= i;
                System.out.println("\nPrime factors: " + i + "\n");
            }
        }

        if (number > 1) {
            System.out.println("Largest prime factor is: " + number + "\n");
        }
        return number;
    }

    public static void tests(long number, int largestPrimeFactor) {
        Assert.assertEquals(getPrimeFactors(number), largestPrimeFactor);
        System.out.println("The largest prime factor for " + number + " is correctly: " + largestPrimeFactor);
    }
}
